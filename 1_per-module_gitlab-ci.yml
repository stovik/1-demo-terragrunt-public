.validate:
  stage: validate
  script:
    - cd ${TERRAGRUNT_ROOT}/${REGION}/${ENVIRONMENT}/${MODULE}
    - terragrunt hclfmt --terragrunt-check
    - terragrunt validate

.plan:
  stage: plan
  script:
    - cd ${TERRAGRUNT_ROOT}/${REGION}/${ENVIRONMENT}/${MODULE}
    - terragrunt plan -input=false -out=${CI_PROJECT_DIR}/terragrunt.tfplan
    - "terragrunt show --json ${CI_PROJECT_DIR}/terragrunt.tfplan 2>/dev/null | convert_report > ${CI_PROJECT_DIR}/terragrunt_plan.json"
    - echo "Finished planning"
  artifacts:
    expire_in: 1 day
    paths:
      - ${CI_PROJECT_DIR}/terragrunt.tfplan
    reports:
      terraform: ${CI_PROJECT_DIR}/terragrunt_plan.json

.apply:
  stage: apply
  script:
    - cd ${TERRAGRUNT_ROOT}/${REGION}/${ENVIRONMENT}/${MODULE}
    - terragrunt apply -input=false -auto-approve ${CI_PROJECT_DIR}/terragrunt.tfplan
  when: manual

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

default:
  image:
    name: misastovicek/terragrunt-tflint-opentofu:v0.1.0
  before_script:
    - |
      #!/bin/bash
      shopt -s expand_aliases
      alias convert_report="jq -r '([.resource_changes[]?.change.actions?]|flatten)|{\"create\":(map(select(.==\"create\"))|length),\"update\":(map(select(.==\"update\"))|length),\"delete\":(map(select(.==\"delete\"))|length)}'"

      command -v ssh-agent || apk --update add openssh-client
      eval $(ssh-agent -s)
      mkdir -p ~/.ssh && chmod 700 ~/.ssh
      echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_ed25519
      ssh-keyscan gitlab.com 2> /dev/null > ~/.ssh/known_hosts
      chmod 600 ~/.ssh/known_hosts && chmod 600 ~/.ssh/id_ed25519
      ssh-add ~/.ssh/id_ed25519
    - az login --service-principal -u $ARM_CLIENT_ID -p $ARM_CLIENT_SECRET --tenant $ARM_TENANT_ID

variables:
  TERRAGRUNT_ROOT: ${CI_PROJECT_DIR}/terragrunt

stages:
  - validate
  - plan
  - apply

tg_validate:prod-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: keyvault-sops
  extends:
    - .validate
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/prod/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan_mr:prod-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: keyvault-sops
  needs:
    - job: tg_validate:prod-westeurope-keyvault-sops
  extends:
    - .plan
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/prod/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan:prod-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: keyvault-sops
  extends:
    - .plan
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/prod/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_apply:prod-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: keyvault-sops
  needs:
    - job: tg_plan:prod-westeurope-keyvault-sops
      artifacts: true
  extends:
    - .apply
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/prod/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_validate:prod-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: mssql-demo
  extends:
    - .validate
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/prod/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan_mr:prod-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: mssql-demo
  needs:
    - job: tg_validate:prod-westeurope-mssql-demo
  extends:
    - .plan
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/prod/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan:prod-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: mssql-demo
  extends:
    - .plan
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/prod/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_apply:prod-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: prod
    MODULE: mssql-demo
  needs:
    - job: tg_plan:prod-westeurope-mssql-demo
      artifacts: true
  extends:
    - .apply
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/prod/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/prod/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_validate:dev-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: keyvault-sops
  extends:
    - .validate
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/dev/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan_mr:dev-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: keyvault-sops
  needs:
    - job: tg_validate:dev-westeurope-keyvault-sops
  extends:
    - .plan
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/dev/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan:dev-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: keyvault-sops
  extends:
    - .plan
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/dev/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_apply:dev-westeurope-keyvault-sops:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: keyvault-sops
  needs:
    - job: tg_plan:dev-westeurope-keyvault-sops
      artifacts: true
  extends:
    - .apply
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/dev/westeurope/keyvault-sops/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/keyvault.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_validate:dev-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: mssql-demo
  extends:
    - .validate
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/dev/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan_mr:dev-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: mssql-demo
  needs:
    - job: tg_validate:dev-westeurope-mssql-demo
  extends:
    - .plan
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - terragrunt/dev/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_plan:dev-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: mssql-demo
  extends:
    - .plan
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/dev/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}

tg_apply:dev-westeurope-mssql-demo:
  variables:
    ENVIRONMENT: westeurope
    REGION: dev
    MODULE: mssql-demo
  needs:
    - job: tg_plan:dev-westeurope-mssql-demo
      artifacts: true
  extends:
    - .apply
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      changes:
        - terragrunt/dev/westeurope/mssql-demo/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/westeurope/*.{hcl,tfvars,yml,yaml}
        - terragrunt/dev/*.{hcl,tfvars,yml,yaml}
        - terragrunt/_common/mssql_server.hcl
        - terragrunt/*.{hcl,tfvars,yml,yaml}
