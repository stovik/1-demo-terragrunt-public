include "root" {
  path = find_in_parent_folders()
}

include "common" {
  path   = "${dirname(find_in_parent_folders())}/_common/mssql_server.hcl"
  expose = true
}

terraform {
  source = "${include.common.locals.base_source_url}?ref=1c403bc"
  before_hook "tflint" {
    commands = ["apply", "plan", "validate"]
    execute  = ["tflint", "--terragrunt-external-tflint", "--minimum-failure-severity=error", "--config", ".tflint.hcl"]
  }
}

inputs = {
  sql_server_name              = "demo"
  administrator_login          = "stovik"
  administrator_login_password = include.common.locals.administrator_login_password
  databases = {
    demo = { sku_name = "GP_S_Gen5_1" }
  }
}
